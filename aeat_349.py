# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.model import ModelView, ModelSQL, fields
from trytond.modules.intrastat.declaration import _get_order_invoice_line_field
from trytond.transaction import Transaction
from sql import Null
from sql.aggregate import Sum
from sql.conditionals import Case, Coalesce


class IntrastatDeclarationLine349(metaclass=PoolMeta):
    __name__ = 'intrastat.declaration.line'

    aeat349_operation_key = fields.Function(
        fields.Many2One('aeat.349.type', 'AEAT 349 Operation Key'),
        'get_aeat349_operation_key', searcher='search_invoice_line_field')

    @classmethod
    def get_aeat349_operation_key(cls, records, name=None):
        return {r.id: r.invoice_line
            and r.invoice_line.aeat349_operation_key
            and r.invoice_line.aeat349_operation_key.id
            or None for r in records
        }

    order_aeat349_operation_key = _get_order_invoice_line_field(
        'aeat349_operation_key')


class Declaration349ComparativeContext(ModelSQL, ModelView):
    '''Context Intrastat declaration Comparative with AEAT 349'''
    __name__ = 'intrastat.declaration.aeat349_comparative.context'

    aeat349_report = fields.Many2One('aeat.349.report', 'AEAT 349 Report',
        required=True)
    intrastat_declaration = fields.Many2One('intrastat.declaration',
        'Intrastat declaration', required=True)
    intrastat_type = fields.Selection([
            ('dispatch', 'Dispatch'),
            ('arrival', 'Arrival')], 'Type', required=True)

    @staticmethod
    def default_intrastat_declaration():
        return Transaction().context.get('intrastat_declaration')

    @staticmethod
    def default_intrastat_type():
        Conf = Pool().get('intrastat.configuration')
        conf = Conf(1)
        if conf.declare_dispatch:
            return 'dispatch'
        elif conf.declare_arrival:
            return 'arrival'
        return 'dispatch'


class Declaration349Comparative(ModelSQL, ModelView):
    '''Intrastat declaration Comparative with AEAT 349'''
    __name__ = 'intrastat.declaration.aeat349_comparative'

    aeat349_report = fields.Many2One('aeat.349.report', 'AEAT 349 Report',
        readonly=True)
    intrastat_declaration = fields.Many2One('intrastat.declaration',
        'Intrastat declaration', readonly=True)
    reason = fields.Selection([
        ('miss_intrastat', 'Missing on Intrastat'),
        ('miss_349', 'Missing on 349'),
        ('wrong_key_349', 'Wrong 349 key'),
        ('amount_diff', 'Amount difference')], 'Reason', readonly=True)
    invoice = fields.Many2One('account.invoice', 'Invoice', readonly=True)
    aeat349_operation_key = fields.Selection('get_aeat349_operation_key',
        'AEAT 349 Operation key', readonly=True)
    intrastat_amount = fields.Numeric('Intrastat Amount',
        digits=(16, Eval('currency_digits', 2)),
        readonly=True, depends=['currency_digits'])
    aeat349_amount = fields.Numeric('AEAT 349 Amount',
        digits=(16, Eval('currency_digits', 2)),
        readonly=True, depends=['currency_digits'])
    currency_digits = fields.Function(
        fields.Integer('Currency digits', readonly=True),
        'get_currency_digits')

    @classmethod
    def get_aeat349_operation_key(cls):
        return Pool().get('aeat.349.type').fields_get(
            ['operation_key'])['operation_key']['selection']

    @classmethod
    def table_query(cls):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')
        DeclarationLine = pool.get('intrastat.declaration.line')
        AEAT349Record = pool.get('aeat.349.record')
        AEAT349Operation = pool.get('aeat.349.report.operation')
        AEAT349Type = pool.get('aeat.349.type')
        invoice = Invoice.__table__()
        invoice_line = InvoiceLine.__table__()
        dline = DeclarationLine.__table__()
        aeat_record = AEAT349Record.__table__()
        aeat_operation = AEAT349Operation.__table__()
        aeat_type = AEAT349Type.__table__()

        mapping = {
            'dispatch': 'E',
            'arrival': 'A'
        }
        context = Transaction().context
        intrastat_subquery = dline.join(invoice_line, condition=(
                invoice_line.id == dline.invoice_line)
            ).join(aeat_type, 'LEFT', condition=(
                invoice_line.aeat349_operation_key == aeat_type.id)
            ).select(
                invoice_line.invoice,
                dline.declaration,
                aeat_type.operation_key,
                Sum(dline.amount).as_('amount'),
                where=(
                    (dline.declaration == context.get('intrastat_declaration'))
                    & (dline.type == context.get('intrastat_type'))
                ),
                group_by=(
                    invoice_line.invoice,
                    dline.declaration,
                    aeat_type.operation_key
                )
        )
        aeat349_subquery = aeat_record.join(aeat_operation, condition=(
                    aeat_record.operation == aeat_operation.id)
            ).select(
                aeat_record.invoice,
                aeat_operation.report,
                aeat_record.operation_key,
                aeat_record.base.as_('amount'),
                where=(
                    (aeat_record.operation_key == mapping.get(
                        context.get('intrastat_type')))
                    & (aeat_operation.report == context.get('aeat349_report')))
            )

        reason = Case(
            (aeat349_subquery.invoice == Null,
                Case((
                        (intrastat_subquery.operation_key == Null)
                        | (intrastat_subquery.operation_key == mapping.get(
                            context.get('intrastat_type'))),
                        'miss_349'),
                    else_='wrong_key_349')),
            else_=Case(
                (intrastat_subquery.invoice == Null, 'miss_intrastat'),
                else_=Case(
                    (intrastat_subquery.amount != aeat349_subquery.amount,
                        'amount_diff'),
                    else_='')
                )
        )
        query = invoice.join(intrastat_subquery, 'LEFT', condition=(
                intrastat_subquery.invoice == invoice.id)
            ).join(aeat349_subquery, 'LEFT', condition=(
                aeat349_subquery.invoice == invoice.id)
            ).select(
                invoice.id,
                invoice.create_date,
                invoice.create_uid,
                invoice.write_date,
                invoice.write_uid,
                intrastat_subquery.declaration.as_('intrastat_declaration'),
                invoice.id.as_('invoice'),
                aeat349_subquery.report.as_('aeat349_report'),
                Coalesce(aeat349_subquery.operation_key,
                    intrastat_subquery.operation_key
                    ).as_('aeat349_operation_key'),
                intrastat_subquery.amount.as_('intrastat_amount'),
                aeat349_subquery.amount.as_('aeat349_amount'),
                reason.as_('reason'),
                where=(
                    (
                        (intrastat_subquery.invoice != Null)
                        | (aeat349_subquery.invoice != Null)
                    ) & (reason != ''))
            )
        return query

    @classmethod
    def get_currency_digits(cls, records, name=None):
        return {r.id: (r.intrastat_declaration or r.aeat349_report
            ).company.currency.digits for r in records}
